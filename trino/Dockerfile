# Copyright (c) Facebook, Inc. and its affiliates.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM azul/zulu-openjdk-debian:11.0.10
EXPOSE 8080

MAINTAINER Piotr Plenik "piotr.plenik@gmail.com"
ARG TRINO_VERSION=355
ENV TRINO_PKG trino-server-$TRINO_VERSION.tar.gz
ENV TRINO_PKG_URL https://repo1.maven.org/maven2/io/trino/trino-server/$TRINO_VERSION/$TRINO_PKG

ENV TRINO_CLI_JAR_URL https://repo1.maven.org/maven2/io/trino/trino-cli/$TRINO_VERSION/trino-cli-${TRINO_VERSION}-executable.jar

# Install python to run the launcher script
RUN apt-get update
RUN apt-get install -y python less curl

# Download Trino package
# Use curl rather ADD <remote> to leverage RUN caching
# Let curl show progress bar to prevent Travis from thinking the job is stalled
RUN curl -o /$TRINO_PKG $TRINO_PKG_URL
RUN tar -zxf /$TRINO_PKG

# Create directory for Trino data
RUN mkdir -p /var/lib/trino/data

# Add Trino configuration
WORKDIR /trino-server-$TRINO_VERSION
RUN mkdir etc
ADD etc/jvm.config etc/
ADD etc/config.properties etc/
ADD etc/node.properties etc/
ADD etc/catalog etc/catalog

# Download Trino CLI
RUN mkdir -p bin
RUN curl -o bin/trino-cli $TRINO_CLI_JAR_URL
RUN chmod +x bin/trino-cli

CMD bin/launcher.py run
